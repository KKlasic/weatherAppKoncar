import React, { useState, useEffect, useRef } from 'react';
import axios from 'axios';
import { DataTable } from 'primereact/datatable';
import { Column } from 'primereact/column';
import Chart from 'chart.js/auto';
import {Dropdown} from 'primereact/dropdown';
import { InputText} from 'primereact/inputtext';
import { Button } from 'primereact/button';
import { Toast } from 'primereact/toast';

function formatDate(dateString) {
  const date = new Date(dateString);
  return date.toLocaleDateString('en-GB', {
    day: '2-digit',
    month: '2-digit',
    year: '2-digit'
  });
}

function App() {
  const [weatherData, setWeatherData] = useState({});
  const [forecastData, setForecastData] = useState({});
  const [loading, setLoading] = useState(false);
  const [locationName, setLocationName] = useState(null);
  const [searchCity, setSearchCity] = useState('');
  const [locationEnabled, setLocationEnabled] = useState(false);
  const chartRef = useRef(null);
  const toast = useRef(null);
  const [selectedFilter, setSelectedFilter] = useState(null);
  const filters = [
      { name: 'Temperature', value: 'temp'},
      { name: 'Tressure', value: 'pressure' },
      { name: 'Humidity', value: 'humidity'},
      { name: 'WindSpeed', value: 'windSpeed'},
  ];

  const tableColumns = [
    { field: 'dt_txt', header: 'Date' },
    { field: 'temp', header: 'Temperature (°C)' },
    { field: 'pressure', header: 'Pressure (hPa)' },
    { field: 'humidity', header: 'Humidity (%)' },
    { field: 'windSpeed', header: 'Wind Speed (m/s)' }
  ];

  useEffect(() => {
    const checkLocationPermission = async () => {
      const permissionStatus = await navigator.permissions.query({ name: 'geolocation' });
      if (permissionStatus.state === 'granted'|| permissionStatus.state === 'prompt') {
        navigator.geolocation.getCurrentPosition(
          async (position) => {
            setLocationEnabled(true);
            const { latitude, longitude } = position.coords;
            try {
              setLoading(true);
              await fetchWeatherData(latitude, longitude);
            } catch (error) {
              console.log(error);
            } finally {
              setLoading(false);
            }
          },
          () => {
            setLocationEnabled(false);
            setLoading(false);
          }
        );
      } else {
        toast.current.show({ severity: 'info', summary: 'Location info', detail: 'Please enable location to view weather data.', life: 3000 });
        setLoading(false);
      }
    };
      checkLocationPermission();
    }, []);

    const filterForecastData = (data) => {
      return data.map((item) => ({
        dt_txt: item.dt_txt,
        [selectedFilter]: item[selectedFilter],
      }))
    }

    useEffect(() => {
      if (forecastData && Object.keys(forecastData).length > 0) {
        const labels = forecastData.list.map(item => item.dt_txt);
        const temperatures = forecastData.list.map(item => item.temp);
        const pressures = forecastData.list.map(item => item.pressure);
        const humidities = forecastData.list.map(item => item.humidity);
        const windSpeeds = forecastData.list.map(item => item.windSpeed);
      
        renderForecastChart(labels, temperatures, pressures, humidities, windSpeeds);
      }
    }, [forecastData, selectedFilter]);
    

  const fetchWeatherData = async (latitude, longitude) => {
    setLoading(true);
    try {
      const locationResponse = await axios.get(
        `https://nominatim.openstreetmap.org/reverse?format=jsonv2&lat=${latitude}&lon=${longitude}`
      );
      setLocationName(locationResponse.data.display_name);
  
      const weatherResponse = await axios.get(
        `https://api.openweathermap.org/data/2.5/weather?lat=${latitude}&lon=${longitude}&appid=b41eb4e6c71a41071fc2a634ceb16e54&units=metric`
      );
      setWeatherData(weatherResponse.data);
  
      setLoading(false);
    } catch (error) {
      toast.current.show({ severity: 'danger', summary: 'Error', detail: 'Error while fetching weather data!', life: 3000 });
      setLoading(false);
    }
  }; 

  const mapForecastData = (data) => {
    const updatedData = data.list.map((item) => {
      return {
        dt_txt: formatDate(item.dt_txt),
        temp: item.main.temp,
        pressure: item.main.pressure,
        humidity: item.main.humidity,
        windSpeed: item.wind.speed,
      }
    });
    return {
      city: data.city,
      list: updatedData,
    }
  };

  const handleSearch = async () => {
    setLoading(true);
    try {
      const response = await axios.get(
        `https://api.openweathermap.org/data/2.5/forecast?q=${searchCity}&appid=b41eb4e6c71a41071fc2a634ceb16e54&units=metric`
      );
      
      setForecastData(mapForecastData(response.data));
      setLoading(false);
    } catch (error) {
      toast.current.show({ severity: 'danger', summary: 'Error', detail: 'Error while fetching data! Contact our support or try again.', life: 3000 });
      setLoading(false);
    }
  };

  const renderForecastGrid = () => {
    if (!forecastData || !forecastData.list || forecastData.list.length === 0) {
      return <div>Enter city for forecast.</div>;
    }
  
    return (
      <div className="forecast-grid">
        <DataTable value={!!selectedFilter ? filterForecastData(forecastData.list) : forecastData.list}>
          {selectedFilter && tableColumns.filter(column => column.field === selectedFilter || column.field === "dt_txt").map((column, index) => (<Column key={index} field={column.field} header={column.header} />))}
          {!selectedFilter && tableColumns.map((column, index) => (<Column key={index} field={column.field} header={column.header} />))}
        </DataTable>
      </div>
    );
  };

  const renderForecastChart = (labels, temperatures, pressures, humidities, windSpeeds) => {
    if (chartRef.current) {
      chartRef.current.destroy();
    }
    const ctx = document.getElementById('forecastChart');
    const datesets = [
      { 
        key: 'temp',
        label: 'Temperature (°C)',
        data: temperatures ,
        borderColor: 'blue',
        fill: false,
      },
      {
        key: 'pressure',
        label: 'Pressure (hPa)',
        data: pressures ,
        borderColor: 'green',
        fill: false,
      },
      {
        key: 'humidity',
        label: 'Humidity (%)',
        data: humidities ,
        borderColor: 'orange',
        fill: false,
      },
      {
        key: 'windSpeed',
        label: 'Wind Speed (m/s)',
        data: windSpeeds,
        borderColor: 'purple',
        fill: false,
      },
    ];

    chartRef.current = new Chart(ctx, {
      type: 'line',
      data: {
        labels: labels,
        datasets: !!selectedFilter ? datesets.filter(item => item.key === selectedFilter) : datesets,
      },
      options: {
        responsive: true,
        maintainAspectRatio: false,
        scales: {
          x: {
            display: true,
            title: {
              display: true,
              text: 'Date',
              color: 'black',
            },
            ticks: {
              color: 'black',
            },
          },
          y: {
            display: true,
            title: {
              display: true,
              text: 'Value',
              color: 'black',
            },
            ticks: {
              color: 'black',
            },
          },
        },
      },
    });
  };

  return (
    <div className="app">
      <div className='topbar'> 
        <div className="topbar-actions">
          <div className='search-bar'>
            <span className="p-input-icon-left">
              <i className="pi pi-search" />
              <InputText value={searchCity} onChange={(e) => setSearchCity(e.target.value)}  onKeyUp={(e) => {
                    if (e.key === "Enter") {
                      handleSearch();
                    }
                  }} 
              placeholder="Enter city name" />
            </span>
            <Button label="Search" onClick={handleSearch} />
          </div>
          <div className='filter-bar'>
          <Dropdown value={selectedFilter} onChange={(e) => setSelectedFilter(e.value)} options={filters} optionLabel="name" optionValue='value'
          placeholder="Select Filter" className='dropdown-filter'/>
          <Button label="Clear" onClick={() => setSelectedFilter(null)} />
          </div>
        </div>
        <div className="container">
          {!loading && Object.keys(weatherData).length === 0 && <p>Enable location to see data</p>}
            {loading && Object.keys(weatherData).length === 0 && <div className='loader-container'>
                  <span className="loader"></span>
            </div>}
            {(!loading || locationEnabled) && weatherData && locationName && (
              <>
                <div className="location">
                  <p className="location-name">{locationName}</p>
                </div>
                <div className="temp">
                  <p className="temperature">{Math.round(weatherData?.main?.temp || 0)}°C</p>
                </div>
                <div className="description">
                  <p className="weather-description">{weatherData?.weather?.[0]?.description}</p>
                </div>
                <div className="weather-info">
                  <div className="feels">
                    <p className="bold weather-info-item">
                      {Math.round(weatherData?.main?.pressure)} hPa</p>
                    <p className="weather-info-label">Pressure</p>
                  </div>
                  <div className="humidity">
                    <p className="bold weather-info-item">{weatherData?.main?.humidity}%</p>
                    <p className="weather-info-label">Humidity</p>
                  </div>
                  <div className="wind">
                    <p className="bold weather-info-item">{weatherData?.wind?.speed} m/s</p>
                    <p className="weather-info-label">Wind Speed</p>
                  </div>
                </div>
                </>
            )}
          </div>
      </div>

      <div className={`forecast-container ${Object.keys(forecastData).length > 0 && "show"}`}>

        {Object.keys(forecastData).length > 0 && (
          <>
            <h2>5 Day / 3 Hour Forecast for {forecastData.city?.name}</h2>
            <div className="forecast-content">
              <div className="chart-container">
                <canvas id="forecastChart"></canvas>
              </div>
              {renderForecastGrid()}
            </div>
            </>
        )}
  
      </div>
      <Toast ref={toast} position="top-center" />
    </div>
  );  
}

export default App;